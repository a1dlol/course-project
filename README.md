# создаем тестовый кластер в aws
1. - eksctl create cluster --name test-cluster --region eu-central-1 --nodegroup-name linux-node --node-type t2.micro --nodes 5
# устанавливаем ingress контроллер   
2. - # helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx #
   - # helm repo update #
   - helm install nginx-ingress ingress-nginx/ingress-nginx
# добавить LoadBalancer в DNS
3.  - kubectl get all
# добавляем gitlab regestry (вначале нужно создать токен в Gitlab)
4. - kubectl create secret docker-registry regcred --docker-server=registry.gitlab.com --docker-username=nickname --docker-password=token --docker-email=email 
   - # или для приватного репозитория команду ниже #
   - # kubectl create secret docker-registry regcred --docker-server=${CI_REGISTRY} --docker-username=${CI_DEPLOY_USER}  --docker-password=${CI_DEPLOY_PASSWORD} --namespace=<your-namespace> #
# создаем namespace (заранее подготовленный yaml)
5. - kubectl apply -f dev-namespace.yaml
   - kubectl apply -f prod-namespace.yaml
# создаем 2 сервисных аккаунта,для dev и prod и токены для них. role и rolebinding общий для двух аккаунтов
6. - kubectl apply -f dev-service-account.yaml
   - kubectl apply -f dev-secret-token.yaml
   - kubectl apply -f prod-service-account.yaml
   - kubectl apply -f prod-secret-token.yaml
   - kubectl apply -f role-and-rolebinding.yaml
# вытаскиеваем cert + ссылку на кластер + токен (все в base64)
7. - kubectl describe secret secret-gitlab -n dev-school
   - kubectl describe secret secret-gitlab -n prod-school
   - kubectl config view --minify -o jsonpath='{.clusters[0].cluster.server}'
   - cat /Users/A118508376/.kube/config 
# готовим переменную KUBECONFIG в gitlab cicd проекта, вставляем полученные выше значения, для dev и prod enviroment.
9. 
```
apiVersion: v1
kind: Config
preferences: {}

clusters:
- cluster:
    certificate-authority-data: $CA
    server: $SERVER
  name: cluster

users:
- name: gitlab
  user:
    token: $TOKEN

contexts:
- context:
    cluster: cluster
    user: gitlab
  name: default

current-context: default
```
# удаляем кластер
10. - eksctl delete cluster --region eu-central-1 --name test-cluster

### полезные команды
- kubectl scale deployment backend-deployment --replicas=0 -n dev-school
- 